package homework.mstruebing.app;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Repository for a user
 *
 */
public class UserRepository extends Repository<User>
{
	public User findById(int id)
	{
		DatabaseService databaseService = new DatabaseService();
		Connection connection = databaseService.getConnection();
		User user = null;

		if (connection != null && id > 0) {
			String stmnt = "SELECT * FROM `" + TABLENAME + "` WHERE id = " + id;

			PreparedStatement pst = null;

			try {
				pst = connection.prepareStatement(stmnt);
				ResultSet rs = pst.executeQuery();
				rs.next();
				user = new User(rs.getInt("id"));
				// works until a user have not more than one passwordlist
				PasswordList passwordList = new PasswordList(user.getId(), user);
				user.setPasswordList(passwordList);
			} catch (SQLException e) {
				System.err.println("ERROR: User not found in database!");
			} finally {
				databaseService.disconnect(connection);
			}
		}

		return user;
	}
}
